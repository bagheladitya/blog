var express=require('express');
var app=express();
var bodyParser  = require('body-parser');
var mongoose=require('mongoose');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

mongoose.connect("mongodb://localhost:27018/data",(err)=>{
    if(err){
        console.log("NOt connected");
    }
    else 
    {
        console.log("connected");
    }
});


app.set('secret','secretkey');

var post=require('./post');
app.use('/',post);
app.listen(4002,()=>{

    console.log('listening at port 4002')
})