var mongoose=require('mongoose');
var post_schema=mongoose.Schema({
   id:{
       type:String,
       required:true
   },
   title:{
       type:String
   },
   post:{
       type:String
   },
   modified_date:{
       type:Date
   },
   added_date:{
       type:Date
   }

});

module.exports=mongoose.model('post',post_schema);
